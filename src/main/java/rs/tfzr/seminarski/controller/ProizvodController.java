package rs.tfzr.seminarski.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import rs.tfzr.seminarski.model.Proizvod;
import rs.tfzr.seminarski.service.ProizvodService;

import java.net.PortUnreachableException;
import java.util.List;

@RestController
@RequestMapping("/proizvod")
public class ProizvodController {

    private ProizvodService proizvodService;

    @Autowired
    public ProizvodController(ProizvodService proizvodService) {
        this.proizvodService = proizvodService;
    }

    @GetMapping
    public List<Proizvod> getAll() {
        return proizvodService.getAll();
    }

    @GetMapping("/{id}")
    public Proizvod getOne(@PathVariable("id") Long id) {
        return proizvodService.getOne(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        proizvodService.delete(id);
    }

    @PostMapping
    public Proizvod save(@RequestBody Proizvod proizvod) {
        return proizvodService.save(proizvod);
    }

    @PatchMapping("/prihod/{id}/{cifra}")
    public Proizvod dodajPrihod(@PathVariable("id") Long id, @PathVariable("cifra") Long cifra) {
        return proizvodService.dodajPrihod(id, cifra);
    }

    @PatchMapping("/rashod/{id}/{cifra}")
    public Proizvod dodajRashod(@PathVariable("id") Long id, @PathVariable("cifra") Long cifra) {
        return proizvodService.dodajRashod(id, cifra);
    }

    @GetMapping("/prihod/{id}")
    public Long dajPrihode(@PathVariable("id") Long id) {
        return proizvodService.dajPrihode(id);
    }

    @GetMapping("/rashod/{id}")
    public Long dajRashode(@PathVariable("id") Long id) {
        return proizvodService.dajRashode(id);
    }

}
