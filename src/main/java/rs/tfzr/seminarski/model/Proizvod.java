package rs.tfzr.seminarski.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.List;

@Entity
@Table
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Proizvod {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String naziv;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Prihod> prihodList;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Rashod> rashodList;

    public Proizvod() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public List<Prihod> getPrihodList() {
        return prihodList;
    }

    public void setPrihodList(List<Prihod> prihodList) {
        this.prihodList = prihodList;
    }

    public List<Rashod> getRashodList() {
        return rashodList;
    }

    public void setRashodList(List<Rashod> rashodList) {
        this.rashodList = rashodList;
    }
}