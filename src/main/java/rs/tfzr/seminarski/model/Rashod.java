package rs.tfzr.seminarski.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Rashod {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Long cifra;

    public Rashod() {
    }

    public Rashod(Long cifra) {
        this.cifra = cifra;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCifra() {
        return cifra;
    }

    public void setCifra(Long cifra) {
        this.cifra = cifra;
    }
}