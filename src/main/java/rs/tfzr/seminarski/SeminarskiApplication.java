package rs.tfzr.seminarski;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SeminarskiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SeminarskiApplication.class, args);
	}

}
