package rs.tfzr.seminarski.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rs.tfzr.seminarski.model.Prihod;

@Repository
public interface PrihodRepository extends JpaRepository<Prihod, Long> {
}
