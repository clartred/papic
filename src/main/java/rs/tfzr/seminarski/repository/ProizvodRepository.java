package rs.tfzr.seminarski.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rs.tfzr.seminarski.model.Proizvod;

@Repository
public interface ProizvodRepository extends JpaRepository<Proizvod, Long> {
}
