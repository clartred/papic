package rs.tfzr.seminarski.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import rs.tfzr.seminarski.model.Rashod;

@Repository
public interface RashodRepository extends JpaRepository<Rashod, Long> {
}
