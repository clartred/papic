package rs.tfzr.seminarski.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rs.tfzr.seminarski.model.Prihod;
import rs.tfzr.seminarski.model.Proizvod;
import rs.tfzr.seminarski.model.Rashod;
import rs.tfzr.seminarski.repository.ProizvodRepository;

import java.util.List;

@Service
public class ProizvodService {

    private ProizvodRepository proizvodRepository;

    @Autowired
    public ProizvodService(ProizvodRepository proizvodRepository) {
        this.proizvodRepository = proizvodRepository;
    }

    public List<Proizvod> getAll() {
        return proizvodRepository.findAll();
    }

    public Proizvod getOne(Long id) {
        return proizvodRepository.getOne(id);
    }

    public Proizvod save(Proizvod proizvod) {
        return proizvodRepository.save(proizvod);
    }

    public void delete(Long id) {
        proizvodRepository.deleteById(id);
    }

    public Proizvod dodajPrihod(Long id, Long cifra) {
        Proizvod proizvod = getOne(id);
        proizvod.getPrihodList().add(new Prihod(cifra));
        return save(proizvod);
    }

    public Proizvod dodajRashod(Long id, Long cifra) {
        Proizvod proizvod = getOne(id);
        proizvod.getRashodList().add(new Rashod(cifra));
        return save(proizvod);
    }

    public Long dajPrihode(Long id) {
        Proizvod proizvod = getOne(id);
        Long prihodi = 0L;
        for (Prihod prihod : proizvod.getPrihodList()) {
            prihodi = prihodi + prihod.getCifra();
        }
        return prihodi;
    }

    public Long dajRashode(Long id) {
        Proizvod proizvod = getOne(id);
        Long rashodi = 0L;
        for (Rashod rashod : proizvod.getRashodList()) {
            rashodi = rashodi + rashod.getCifra();
        }
        return rashodi;
    }
}
